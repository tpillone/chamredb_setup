

nextflow.enable.dsl = 2
params.output = "data" 

process download_card {
    publishDir "${params.output}/card", mode: "copy"
    output:
      path 'card.metadata.obo'
      path 'protein_fasta_protein_homolog_model.fasta'
      path 'nucleotide_fasta_protein_homolog_model.fasta'
    script:
		"""
		wget https://github.com/arpcard/aro/raw/master/src/ontology/aro.obo -O card.metadata.obo
		wget https://card.mcmaster.ca/latest/data
		tar -xvf data ./protein_fasta_protein_homolog_model.fasta
		tar -xvf data ./nucleotide_fasta_protein_homolog_model.fasta
      """
}

 process download_resfinder {
    publishDir "${params.output}/resfinder", mode: "copy"
    output:
	  path 'resfinder.metadata.tsv'
	  path 'resfinder.nucl.fasta'
    script:
      """
    wget https://bitbucket.org/genomicepidemiology/resfinder_db/raw/master/phenotypes.txt -O resfinder.metadata.tsv
    git clone https://bitbucket.org/genomicepidemiology/resfinder_db data
    cat data/*.fsa > resfinder.nucl.fasta
      """
}

workflow {
    download_card()
    download_resfinder()
}
